package pl.monku.demoforelenx

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
class DemoForElenxApplication


fun main(args: Array<String>) {
    SpringApplication.run(DemoForElenxApplication::class.java, *args)
}


